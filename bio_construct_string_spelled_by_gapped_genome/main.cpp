#include <iostream>
#include <vector>

int main() {
    freopen("input.txt", "r", stdin);
    size_t k, d;
    std::cin >> k >> d;
    std::string s;
    std::vector<char> ans(1000000);
    size_t count = 0;
    while (std::cin >> s) {
        for (size_t j = 0; j < s.length() + d - 1; ++j) {
            if (j < k) {
                ans[count + j] = s[j];
            }
            if (j >= k + d) {
                ans[count + j] = s[j - d + 1];
            }
        }
        count ++;
    }
    for (size_t i = 0; i < 2 * k + d + count - 1; ++i) {
        std::cout << ans[i];
    }
}