#include <iostream>
#include <vector>
#include <math.h>

size_t CalculateDistance(std::string &first, std::string &second) {
    size_t mismatch_number = 0;
    for (size_t i = 0; i < first.size(); ++i) {
        if (first[i] != second[i]) {
            ++mismatch_number;
        }
    }
    return mismatch_number;
}

size_t CalculateFreq(std::string &genom, std::string &pattern, int max_mismatch) {
    size_t ans = 0;
    for (size_t i = 0; i < genom.length() - pattern.length() + 1; ++i) {
        std::string cur_part = genom.substr(i, pattern.length());
        if (CalculateDistance(cur_part, pattern) <= max_mismatch) {
            ++ans;
        }
    }
    return ans;
}

std::string int_to_pattern(size_t number, int length) {
    std::vector<char> chars = {'A', 'C', 'G', 'T'};
    size_t cur = number;
    std::string pattern = "";
    for (int i = 0; i < length; ++i) {
        pattern += chars[cur % 4];
        cur /= 4;
    }
    return pattern;
}

int main() {
    freopen("input.txt", "r", stdin);
    std::string genom;
    int length, max_mismatch;
    std::cin >> genom;
    std::cin >> length >> max_mismatch;
    size_t max_freq = 0;
    std::vector<std::string> ans;
    for (size_t i = 0; i < pow(4, length); ++i) {
        std::string cur_pattern = int_to_pattern(i, length);
        size_t cur_freq = CalculateFreq(genom, cur_pattern, max_mismatch);
        //std::cout << cur_pattern << ' ' << cur_freq << '\n';
        if (cur_freq > max_freq) {
            max_freq = cur_freq;
            ans.clear();
        }
        if (cur_freq == max_freq) {
            ans.push_back(cur_pattern);
        }
    }
    for (auto pattern : ans) {
        std::cout << pattern << ' ';
    }
}