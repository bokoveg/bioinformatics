#include <iostream>

size_t CalculateDistance(std::string first, std::string second) {
    size_t mismatch_number = 0;
    for (size_t i = 0; i < first.size(); ++i) {
        if (first[i] != second[i]) {
            ++mismatch_number;
        }
    }
    return mismatch_number;
}

int main() {
    freopen("input.txt", "r", stdin);
    std::string genom, pattern;
    std::cin >> pattern >> genom;
    size_t max_mismatch_number;
    std::cin >> max_mismatch_number;
    for (size_t i = 0; i < genom.size() - pattern.size() + 1; ++i) {
        std::string part = genom.substr(i, pattern.size());
        if (CalculateDistance(part, pattern) <= max_mismatch_number) {
            std::cout << i << ' ';
        }
    }
}
