#include <iostream>
#include <vector>
#include <algorithm>

struct Node {
    int id;
    std::string tag;
};

bool HasEdge(std::string &first, std::string &second, size_t n) {
    return first.substr(0, n - 1) == second.substr(1, n - 1) &&
           first.substr(n + 1, n - 1) == second.substr(n + 2, n - 1);
}

class Graph {
public:
    Graph(std::vector<std::vector<bool> > &matrix, std::vector<Node> &nodes) {
        matrix_ = matrix;
        nodes_ = nodes;
    }

    std::vector<Node> GetEulerPath(size_t start_index) {
        std::vector<Node> path;
        std::vector<std::vector<bool> > matrix = matrix_;
        Step(matrix, path, start_index);
        std::reverse(path.begin(), path.end());
        return path;
    }

    std::vector<Node> GetEulerCycle() {
        std::vector<Node> path;
        std::vector<std::vector<bool> > matrix = matrix_;
        Step(matrix, path, 0);
        std::reverse(path.begin(), path.end());
        return path;
    }

    std::vector<Node> FindHamiltonPath(size_t index) {
        std::vector<Node> path;
        std::vector<bool> was(nodes_.size(), false);
        path.push_back(nodes_[index]);
        DFS(index, path, was);
        std::reverse(path.begin(), path.end());
        return path;
    }

private:
    bool DFS(size_t index, std::vector<Node> &path, std::vector<bool> &was) {
        /*std::cout << index << '\n';
        was[index] = true;
        for (size_t i = 0; i < path.size(); ++i) {
            std::cout << path[i].tag << ' ';
        }
        std::cout << '\n';*/
        for (size_t i = 0; i < nodes_.size(); ++i) {
            if (matrix_[index][i] && !was[i]) {
                path.push_back(nodes_[i]);
                if (path.size() == nodes_.size() || DFS(i, path, was)) {
                    return true;
                } else {
                    path.pop_back();
                    was[index] = false;
                    return false;
                }
            }
        }
    }


    void Step(std::vector<std::vector<bool>> &matrix, std::vector<Node> &path, size_t index) {
        for (size_t i = 0; i < nodes_.size(); ++i) {
            if (matrix[index][i]) {
                matrix[index][i] = false;
                Step(matrix, path, i);
            }
        }
        path.push_back(nodes_[index]);
    }

    std::vector<std::vector<bool> > matrix_;
    std::vector<Node> nodes_;
};

std::vector<Node> GenerateNodes(size_t n) {
    std::vector<Node> nodes;
    for (size_t i = 0; i < (1 << n); ++i) {
        std::string s = "";
        size_t k = i;
        for (size_t j = 0; j < n; ++j) {
            s += '0' + k % 2;
            k /= 2;
        }
        nodes.push_back({0, s});
    }
    return nodes;
}

std::string ConstructString(std::vector<Node> nodes, size_t k, size_t d) {
    std::string ans;
    std::vector<char> chars(1000000);
    size_t count = 0;
    std::vector<std::string> node_strings;
    for (Node node : nodes) {
        node_strings.push_back(node.tag);
    }
    for (std::string s : node_strings) {
        for (size_t j = 0; j < s.length() + d - 1; ++j) {
            if (j < k) {
                chars[count + j] = s[j];
            }
            if (j >= k + d) {
                chars[count + j] = s[j - d + 1];
            }
        }
        count ++;
    }
    for (size_t i = 0; i < 2 * k + d + count - 1; ++i) {
        ans += chars[i];
    }
    return ans;
}

int main() {
    freopen("input.txt", "r", stdin);
    size_t k, d;
    std::cin >> k >> d;
    std::string line;
    std::vector<Node> nodes;
    while (std::cin >> line) {
        nodes.push_back({0, line});
    }
    // std::cout << nodes.size();
    std::vector<std::vector<bool> > matrix(nodes.size(), std::vector<bool>(nodes.size(), false));
    size_t start_index = 0;
    std::vector<size_t> in_deg(nodes.size(), 0);
    std::vector<size_t> out_deg(nodes.size(), 0);

    for (size_t i = 0; i < matrix.size(); ++i) {
        for (size_t j = 0; j < matrix.size(); ++j) {
            if (i != j ) {
                matrix[i][j] = HasEdge(nodes[i].tag, nodes[j].tag, k);
                if (matrix[i][j]) {
                    in_deg[j]++;
                    out_deg[i]++;
                }
            }
        }
    }
    for (size_t i = 0; i < nodes.size(); ++i) {
        if (out_deg[i] > in_deg[i]) {
            start_index = i;
            break;
        }
    }


    Graph g(matrix, nodes);
    std::vector<Node> path = g.FindHamiltonPath(start_index);
    std::string ans = ConstructString(path, k, d);
    std::cout << ans;
}