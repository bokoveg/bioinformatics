#include <iostream>
#include <algorithm>
#include <vector>

std::vector<int> origin_masses =
        {0, 57, 71, 87, 97, 99, 101, 103, 113, 114, 115, 128, 129, 131, 137, 147, 156, 163, 186 };
std::vector<size_t> allowed_masses;

std::vector<size_t> CalculateSpectrum(std::vector<size_t> &masses) {
    std::vector<size_t> ans;
    ans.push_back(0);
    for (size_t length = 1; length < masses.size(); ++length) {
        size_t sum = 0;
        for (size_t i = 0; i < length; ++i) {
            sum += masses[i];
        }
        ans.push_back(sum);
        for (size_t start = 1; start < masses.size(); ++start) {
            sum += masses[(start + length - 1) % masses.size()];
            sum -= masses[start - 1];
            ans.push_back(sum);
        }
    }
    size_t sum = 0;
    for (auto elem : masses) {
        sum += elem;
    }
    ans.push_back(sum);
    std::sort(ans.begin(), ans.end());
    return ans;
}

void PrintPermutation(std::vector<size_t> &masses) {
    if (masses.size() > 0) {
        for (size_t i = 0; i < masses.size() - 1; ++i) {
            std::cout << masses[i] << '-';
        }
        std::cout << masses[masses.size() - 1] << ' ';
    }
}

struct Peptide {
    std::vector<size_t> masses;
    size_t sum_mass;

    std::vector<Peptide> expand(size_t limit) {
        std::vector<Peptide> new_peptides;
        for (auto mass : allowed_masses) {
            if (sum_mass + mass <= limit) {
                new_peptides.push_back(Peptide(*this, mass));
            }
        }
        return new_peptides;
    }

    Peptide (Peptide &another, size_t add) {
        masses = another.masses;
        masses.push_back(add);
        sum_mass = another.sum_mass + add;
    }

    Peptide () {
        masses = std::vector<size_t>();
        sum_mass = 0;
    }

    size_t score(std::vector<size_t> &spectrum) {
        std::vector<size_t> my_spectrum = CalculateSpectrum(masses);
        std::vector<size_t> count(std::max(my_spectrum[my_spectrum.size() - 1],
                                           spectrum[spectrum.size() - 1]) + 1, 0);
        for (auto value : spectrum) {
            count[value] ++;
        }
        size_t score = 0;
        for (auto value : my_spectrum) {
            if (count[value] > 0) {
                ++score;
                --count[value];
            }
        }
        return score;
    }

    void print() {
        PrintPermutation(masses);
    }

    bool operator < (const Peptide &other) const {
        return masses < other.masses;
    }
};

bool CheckPermutation(std::vector<size_t> &masses, std::vector<size_t> &spectrum) {
    std::vector<size_t> calulated_spectrum = CalculateSpectrum(masses);
    // PrintPermutation(calulated_spectrum);
    return calulated_spectrum == spectrum;
}

Peptide FindLeaderPeptide(std::vector<size_t> &spectrum, size_t N) {
    std::vector<std::pair<size_t, Peptide > > leaderboard;
    size_t limit = spectrum[spectrum.size() - 1];
    leaderboard.push_back(std::make_pair(0, Peptide()));
    Peptide leader_peptide;
    size_t best_score = 0;
    while (leaderboard.size() > 0) {
        /* std::cout << leaderboard.size() << '\n';
        for (size_t i = 0; i < leaderboard.size(); ++i) {
            std::cout << leaderboard[i].first << ' ';
            leaderboard[i].second.print();
            std::cout << '\n';
        } */
        std::vector<std::pair<size_t, Peptide > > new_leaderboard;
        for (auto scored_peptide : leaderboard) {
            std::vector<Peptide> expanded = scored_peptide.second.expand(limit);
            for (auto peptide : expanded) {
                size_t score = peptide.score(spectrum);
                if (peptide.sum_mass == limit && score > best_score) {
                    leader_peptide = peptide;
                    best_score = score;
                }
                new_leaderboard.push_back(std::make_pair(score, peptide));
            }
        }
        std::sort(new_leaderboard.begin(), new_leaderboard.end());
        std::reverse(new_leaderboard.begin(), new_leaderboard.end());
        leaderboard.clear();
        for (size_t i = 0; i < std::min(N, new_leaderboard.size()); ++i) {
            leaderboard.push_back(new_leaderboard[i]);
        }
    }
    return leader_peptide;
}

void CalculateAllowedMasses(size_t M, std::vector<size_t> &spectrum) {
    std::vector<int> count(200);
    for (size_t i = 0; i < spectrum.size(); ++i) {
        for (size_t j = i + 1; j < spectrum.size(); ++j) {
            if (spectrum[j] - spectrum[i] >= 57 && spectrum[j] - spectrum[i] <= 200) {
                count[spectrum[j] - spectrum[i]]++;
            }
        }
    }
    std::vector<std::pair<int, size_t> > tops;
    for (size_t i = 0; i < count.size(); ++i) {
        tops.push_back(std::make_pair(-count[i], i));
    }
    std::sort(tops.begin(), tops.end());
    for (size_t i = 0; i < M; ++i) {
        allowed_masses.push_back(tops[i].second);
    }
}

int main() {
    freopen("input.txt", "r", stdin);
    size_t M, N, number;
    std::vector<size_t> spectrum;
    std::cin >> M >> N;
    while (std::cin >> number) {
        spectrum.push_back(number);
    }
    std::sort(spectrum.begin(), spectrum.end());
    CalculateAllowedMasses(M, spectrum);
    FindLeaderPeptide(spectrum, N).print();
    /* for (size_t i = 0; i < allowed_masses.size(); ++i) {
        std::cout << allowed_masses[i] << ' ';
    }*/
}
