#include <iostream>
#include <algorithm>
#include <vector>

const std::vector<size_t> allowed_masses =
        { 57, 71, 87, 97, 99, 101, 103, 113, 114, 115, 128, 129, 131, 137, 147, 156, 163, 186 };

std::vector<size_t> CalculateSpectrum(std::vector<size_t> &masses) {
    std::vector<size_t> ans;
    ans.push_back(0);
    for (size_t length = 1; length < masses.size(); ++length) {
        size_t sum = 0;
        for (size_t i = 0; i < length; ++i) {
            sum += masses[i];
        }
        ans.push_back(sum);
        for (size_t start = 1; start < masses.size(); ++start) {
            sum += masses[(start + length - 1) % masses.size()];
            sum -= masses[start - 1];
            ans.push_back(sum);
        }
    }
    size_t sum = 0;
    for (auto elem : masses) {
        sum += elem;
    }
    ans.push_back(sum);
    std::sort(ans.begin(), ans.end());
    return ans;
}

void PrintPermutation(std::vector<size_t> &masses) {
    for (size_t i = 0; i < masses.size() - 1; ++i) {
        std::cout << masses[i] << '-';
    }
    std::cout << masses[masses.size() - 1] << ' ';
}

bool CheckPermutation(std::vector<size_t> &masses, std::vector<size_t> &spectrum) {
    std::vector<size_t> calulated_spectrum = CalculateSpectrum(masses);
    // PrintPermutation(calulated_spectrum);
    return calulated_spectrum == spectrum;
}



int main() {
    freopen("input.txt", "r", stdin);
    size_t number;
    std::vector<size_t> masses;
    std::vector<size_t> spectrum;
    while (std::cin >> number) {
        spectrum.push_back(number);
        for (auto cur_mass : allowed_masses) {
            if (cur_mass == number) {
                masses.push_back(number);
            }
        }
    }
    std::sort(masses.begin(), masses.end());
    std::cout << masses.size() << '\n';
    do {
        if (CheckPermutation(masses, spectrum)) {
            PrintPermutation(masses);
        }
    } while (std::next_permutation(masses.begin(), masses.end()));
}
