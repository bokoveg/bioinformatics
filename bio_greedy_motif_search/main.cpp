#include <iostream>
#include <vector>
#include <string>
#include <map>

class Profile {
public:
    Profile(size_t motif_length) {
        motif_length_ = motif_length;
        std::map<char, size_t> start_dist;
        start_dist['A'] = 0;
        start_dist['T'] = 0;
        start_dist['G'] = 0;
        start_dist['C'] = 0;
        for (size_t i = 0; i < motif_length; ++i) {
            dist_.push_back(start_dist);
        }
    }

    void Add(std::string &motif) {
        motifs_.push_back(motif);
        for (size_t i = 0; i < motif_length_; ++i) {
            dist_[i][motif[i]] ++;
        }
    }

    size_t CalculateProb(std::string &motif) {
        size_t res = 1;
        for (size_t i = 0; i < motif_length_; ++i) {
            res *= dist_[i][motif[i]];
        }
        return res;
    }

    std::string FindBestMotif(std::string &genom) {
        size_t best_res = 0;
        std::string best_motif = genom.substr(0, motif_length_);
        for (size_t i = 0; i < genom.length() + 1 - motif_length_; ++i) {
            std::string cur_motif = genom.substr(i, motif_length_);
            size_t cur_res = CalculateProb(cur_motif);
            if (cur_res > best_res) {
                best_res = cur_res;
                best_motif = cur_motif;
            }
        }
        return best_motif;
    }

    size_t Score() {
        size_t score = motif_length_ * motifs_.size();
        for (size_t i = 0; i < motif_length_; ++i) {
            score -= std::max(std::max(dist_[i]['A'], dist_[i]['C']),
                              std::max(dist_[i]['T'], dist_[i]['G']));
        }
        return score;
    }

    void Print() {
        for (auto motif : motifs_) {
            std::cout << motif << '\n';
        }
    }
private:
    std::vector<std::string> motifs_;
    std::vector<std::map<char, size_t> > dist_;
    size_t motif_length_;
};

int main() {
    freopen("input.txt", "r", stdin);

    size_t motif_length, genom_number;
    std::vector<std::string> genoms;
    std::cin >> motif_length >> genom_number;
    for (size_t i = 0; i < genom_number; ++i) {
        std::string s;
        std::cin >> s;
        genoms.push_back(s);
    }
    bool inited = false;
    Profile best_profile(motif_length);
    for (size_t i = 0; i < genoms[0].length() + 1 - motif_length; ++i) {
        Profile cur_profile(motif_length);
        std::string start_string = genoms[0].substr(i, motif_length);
        cur_profile.Add(start_string);
        for (size_t j = 1; j < genoms.size(); ++j) {
            std::string cur_best_motif = cur_profile.FindBestMotif(genoms[j]);
            cur_profile.Add(cur_best_motif);
        }
        //std::cout << cur_profile.Score() << '\n';
        if (cur_profile.Score() < best_profile.Score() || !inited) {
            inited = true;
            best_profile = cur_profile;
        }
    }
    best_profile.Print();
}
