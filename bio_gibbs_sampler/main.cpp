#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <ctime>
#include <random>

size_t GenerateNumber(std::vector<size_t> &p) {
    std::vector<size_t> sums;
    sums.push_back(0);
    for (int i = 0; i < p.size(); ++i) {
        sums.push_back(sums[sums.size() - 1] + p[i]);
    }
    size_t x = rand() % sums[p.size()];
    size_t ans = 0;
    while (sums[ans] <= x) {
        ans ++;
    }
    return ans;
}

class Profile {
public:
    Profile(size_t motif_length) {
        motif_length_ = motif_length;
        std::map<char, size_t> start_dist;
        start_dist['A'] = 1;
        start_dist['T'] = 1;
        start_dist['G'] = 1;
        start_dist['C'] = 1;
        for (size_t i = 0; i < motif_length; ++i) {
            dist_.push_back(start_dist);
        }
    }

    void Add(std::string &motif) {
        motifs_.push_back(motif);
        for (size_t i = 0; i < motif_length_; ++i) {
            dist_[i][motif[i]] ++;
        }
    }

    size_t CalculateProb(std::string &motif) {
        size_t res = 1;
        for (size_t i = 0; i < motif_length_; ++i) {
            res *= dist_[i][motif[i]];
        }
        return res;
    }

    std::string FindBestMotif(std::string &genom) {
        size_t best_res = 0;
        std::string best_motif = genom.substr(0, motif_length_);
        for (size_t i = 0; i < genom.length() + 1 - motif_length_; ++i) {
            std::string cur_motif = genom.substr(i, motif_length_);
            size_t cur_res = CalculateProb(cur_motif);
            if (cur_res > best_res) {
                best_res = cur_res;
                best_motif = cur_motif;
            }
        }
        return best_motif;
    }

    void UpdateMotif(std::string &genom, size_t index) {
        Profile profile(motif_length_);
        for (size_t i = 0; i < index; ++i) {
            profile.Add(motifs_[i]);
        }
        for (size_t i = index + 1; i < motifs_.size(); ++i) {
            profile.Add(motifs_[i]);
        }
        std::vector<size_t> probs;
        for (size_t i = 0; i < genom.length() - motif_length_ + 1; ++i ) {
            std::string s = genom.substr(i, motif_length_);
            probs.push_back(profile.CalculateProb(s));
        }
        size_t x = GenerateNumber(probs);
        /*std::cout << x << '\n';
        for (size_t i = 0; i < probs.size(); ++i) {
            std::cout << probs[i] << ' ';
        }
        std::cout << '\n';*/
        motifs_[index] = genom.substr(x, motif_length_);
    }

    size_t Score() {
        size_t score = motif_length_ * motifs_.size();
        for (size_t i = 0; i < motif_length_; ++i) {
            score -= std::max(std::max(dist_[i]['A'], dist_[i]['C']),
                              std::max(dist_[i]['T'], dist_[i]['G'])) - 1;
        }
        if (motifs_.size() > 0) {
            return score;
        } else {
            return -1;
        }
    }

    void Print() {
        for (auto motif : motifs_) {
            std::cout << motif << '\n';
        }
    }

    size_t GetMotifLength() {
        return motif_length_;
    }

    void Remove(size_t i) {
        std::string motif = motifs_[i];
        std::swap(motifs_[i], motifs_[motifs_.size() - 1]);
        for (size_t j = 0; j < motif_length_; ++j) {
            dist_[i][motif[i]] --;
        }
    }

private:
    std::vector<std::string> motifs_;
    std::vector<std::map<char, size_t> > dist_;
    size_t motif_length_;
};



Profile GenerateRandomProfile(std::vector<std::string> &genoms, size_t motif_length) {
    Profile res(motif_length);
    for (size_t i = 0; i < genoms.size(); ++i) {
        size_t x = rand() % (genoms[0].length() - motif_length + 1);
        std::string motif = genoms[i].substr(x, motif_length);
        res.Add(motif);
    }
    return res;
}

std::string GenerateRandomMotif(std::string &genom, size_t motif_length) {
    size_t x = rand() % (genom.length() - motif_length + 1);
    std::string motif = genom.substr(x, motif_length);
    return motif;
}

Profile UpdateProfile(Profile &profile, std::vector<std::string> &genoms) {
    Profile new_profile(profile.GetMotifLength());
    for (size_t i = 0; i < genoms.size(); ++i) {
        std::string s = profile.FindBestMotif(genoms[i]);
        new_profile.Add(s);
    }
    return new_profile;
}

Profile GibbsSampler(std::vector<std::string> &genoms, size_t motif_length, size_t test_num) {
    Profile profile = GenerateRandomProfile(genoms, motif_length);
    Profile best_profile = profile;
    for (size_t j = 0; j < test_num; ++j) {
        size_t removed_index = rand() % genoms.size();
        profile.UpdateMotif(genoms[removed_index], removed_index);
        if (profile.Score() < best_profile.Score()) {
            best_profile = profile;
        }
    }
    return best_profile;
}

int main() {
    srand(time(nullptr));
    freopen("input.txt", "r", stdin);

    size_t motif_length, genom_number, test_num;
    std::vector<std::string> genoms;
    std::cin >> motif_length >> genom_number >> test_num;
    for (size_t i = 0; i < genom_number; ++i) {
        std::string s;
        std::cin >> s;
        genoms.push_back(s);
    }
    Profile best_profile(motif_length);
    for (size_t i = 0; i < 100; ++i) {
        Profile profile = GibbsSampler(genoms, motif_length, test_num);
        // std::cout << profile.Score() << '\n';
        if (best_profile.Score() > profile.Score()) {
            best_profile = profile;
        }
    }
    best_profile.Print();
    std::cout << best_profile.Score();
}