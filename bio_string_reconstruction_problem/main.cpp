#include <iostream>
#include <vector>
#include <algorithm>

struct Node {
    int id;
    std::string tag;
};

bool HasEdge(std::string &first, std::string &second) {
    return first.substr(1, first.size() - 1) == second.substr(0, second.size() - 1);
}

class Graph {
public:
    Graph(std::vector<std::vector<bool> > &matrix, std::vector<Node> &nodes) {
        matrix_ = matrix;
        nodes_ = nodes;
    }

    std::vector<Node> GetEulerPath(size_t start_index) {
        std::vector<Node> path;
        std::vector<std::vector<bool> > matrix = matrix_;
        Step(matrix, path, start_index);
        std::reverse(path.begin(), path.end());
        return path;
    }

private:
    void Step(std::vector<std::vector<bool>> &matrix, std::vector<Node> &path, size_t index) {
        for (size_t i = 0; i < nodes_.size(); ++i) {
            if (matrix[index][i]) {
                matrix[index][i] = false;
                Step(matrix, path, i);
            }
        }
        path.push_back(nodes_[index]);
    }

    std::vector<std::vector<bool> > matrix_;
    std::vector<Node> nodes_;
};

int main() {
    freopen("input.txt", "r", stdin);
    size_t n = 0;
    std::cin >> n;
    std::string line;
    std::vector<Node> nodes;
    while (std::cin >> line) {
        nodes.push_back({0, line});
    }
    // std::cout << nodes.size();
    std::vector<std::vector<bool> > matrix(nodes.size(), std::vector<bool>(nodes.size(), false));
    size_t start_index = 0;
    std::vector<size_t> in_deg(nodes.size(), 0);
    std::vector<size_t> out_deg(nodes.size(), 0);

    for (size_t i = 0; i < matrix.size(); ++i) {
        for (size_t j = 0; j < matrix.size(); ++j) {
            if (i != j ) {
                matrix[i][j] = HasEdge(nodes[i].tag, nodes[j].tag);
                if (matrix[i][j]) {
                    in_deg[j]++;
                    out_deg[i]++;
                }
            }
        }
    }
    for (size_t i = 0; i < nodes.size(); ++i) {
        if (out_deg[i] > in_deg[i]) {
            start_index = i;
            break;
        }
    }


    Graph g(matrix, nodes);
    std::vector<Node> path = g.GetEulerPath(start_index);
    std::string ans = path[0].tag;
    for (size_t i = 1; i < matrix.size(); ++i) {
        ans += path[i].tag[n - 1];
    }
    std::cout << ans;
}