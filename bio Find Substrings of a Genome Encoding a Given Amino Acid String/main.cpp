#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>

const std::map<std::string, char> codon_acid_map = {
        { "GCA", 'A' },
        { "GCC", 'A' },
        { "GCG", 'A' },
        { "GCT", 'A' },
        { "TGC", 'C' },
        { "TGT", 'C' },
        { "GAC", 'D' },
        { "GAT", 'D' },
        { "GAA", 'E' },
        { "GAG", 'E' },
        { "TTC", 'F' },
        { "TTT", 'F' },
        { "GGA", 'G' },
        { "GGC", 'G' },
        { "GGG", 'G' },
        { "GGT", 'G' },
        { "CAC", 'H' },
        { "CAT", 'H' },
        { "ATA", 'I' },
        { "ATC", 'I' },
        { "ATT", 'I' },
        { "AAA", 'K' },
        { "AAG", 'K' },
        { "CTA", 'L' },
        { "CTC", 'L' },
        { "CTG", 'L' },
        { "CTT", 'L' },
        { "TTA", 'L' },
        { "TTG", 'L' },
        { "ATG", 'M' },
        { "AAC", 'N' },
        { "AAT", 'N' },
        { "CCA", 'P' },
        { "CCC", 'P' },
        { "CCG", 'P' },
        { "CCT", 'P' },
        { "CAA", 'Q' },
        { "CAG", 'Q' },
        { "AGA", 'R' },
        { "AGG", 'R' },
        { "CGA", 'R' },
        { "CGC", 'R' },
        { "CGG", 'R' },
        { "CGT", 'R' },
        { "AGC", 'S' },
        { "AGT", 'S' },
        { "TCA", 'S' },
        { "TCC", 'S' },
        { "TCG", 'S' },
        { "TCT", 'S' },
        { "ACA", 'T' },
        { "ACC", 'T' },
        { "ACG", 'T' },
        { "ACT", 'T' },
        { "GTA", 'V' },
        { "GTC", 'V' },
        { "GTG", 'V' },
        { "GTT", 'V' },
        { "TGG", 'W' },
        { "TAC", 'Y' },
        { "TAT", 'Y' },
        { "TAG", '#' },
        { "TGA", '#' },
        { "TAA", '#' },
};

std::string reverse_complement(const std::string& line) {
    std::string result = line;
    for (size_t i = 0; i < line.length(); ++i) {
        switch (line[i]) {
            case 'A':
                result[i] = 'T';
                break;
            case 'C':
                result[i] = 'G';
                break;
            case 'G':
                result[i] = 'C';
                break;
            case 'T':
                result[i] = 'A';
                break;
        }
    }
    reverse(result.begin(), result.end());
    return result;
}

bool CheckCodon(std::string codon, std::string &etalon, size_t acid_len) {
    std::string acid_res = "";
    for (size_t i = 0; i < acid_len; ++i) {
        acid_res += codon_acid_map.at(codon.substr(i * 3, 3));
    }
    return acid_res == etalon;
}

int main() {
    freopen("input.txt", "r", stdin);
    std::string codon_line, acid_line;
    std::cin >> codon_line;
    std::cin >> acid_line;
    for (size_t i = 0; i < codon_line.size() - 3 * acid_line.size() + 1; ++i) {
        std::string cur_codon = codon_line.substr(i, 3 * acid_line.size());
        if (CheckCodon(cur_codon, acid_line, acid_line.size()) ||
            CheckCodon(reverse_complement(cur_codon), acid_line, acid_line.size())) {
            std::cout << cur_codon << '\n';
        }
    }
    return 0;
}