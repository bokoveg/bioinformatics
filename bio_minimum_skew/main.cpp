#include <iostream>
#include <string>
#include <vector>

int main() {
    freopen("input.txt", "r", stdin);
    std::string genom;
    std::cin >> genom;
    std::vector<int> ans;
    int min_skew_value = 0;
    int cur_skew_value = 0;
    ans.push_back(0);
    for (int i = 0; i < genom.length(); ++i) {
        //std::cout << cur_skew_value << ' ' << min_skew_value << '\n';
        if (genom[i] == 'C') {
            --cur_skew_value;
        }
        if (genom[i] == 'G') {
            ++cur_skew_value;
        }
        if (cur_skew_value < min_skew_value) {
            min_skew_value = cur_skew_value;
            ans.clear();
        }
        if (cur_skew_value == min_skew_value) {
            ans.push_back(i);
        }
    }
    for (auto index : ans) {
        std::cout << index + 1 << ' ';
    }
}