cmake_minimum_required(VERSION 3.6)
project(generate_contigs_from_collection_reads)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(generate_contigs_from_collection_reads ${SOURCE_FILES})