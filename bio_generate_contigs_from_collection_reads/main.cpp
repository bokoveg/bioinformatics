#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

struct Node {
    int id;
    std::string tag;
};

bool HasEdge(std::string &first, std::string &second, size_t n) {
    return first.substr(0, n - 1) == second.substr(1, n - 1) &&
           first.substr(n + 1, n - 1) == second.substr(n + 2, n - 1);
}

class Graph {
public:
    Graph(std::vector<std::vector<int> > &matrix, std::vector<Node> &nodes) {
        matrix_ = matrix;
        nodes_ = nodes;
    }

    std::vector<Node> GetEulerPath(size_t start_index) {
        std::vector<Node> path;
        std::vector<std::vector<int> > matrix = matrix_;
        Step(matrix, path, start_index);
        std::reverse(path.begin(), path.end());
        return path;
    }

    std::vector<Node> GetEulerCycle() {
        std::vector<Node> path;
        std::vector<std::vector<int> > matrix = matrix_;
        Step(matrix, path, 0);
        std::reverse(path.begin(), path.end());
        return path;
    }

    std::vector<Node> FindHamiltonPath(size_t index) {
        std::vector<Node> path;
        std::vector<bool> was(nodes_.size(), false);
        path.push_back(nodes_[index]);
        DFS(index, path, was);
        std::reverse(path.begin(), path.end());
        return path;
    }

    void PrintNodes() {
        for (size_t i = 0; i < nodes_.size(); ++i) {
            for (size_t j = 0; j < nodes_.size(); ++j) {
                for (size_t k = 0; k < matrix_[i][j]; ++k) {
                    std::cout << nodes_[i].tag << nodes_[j].tag[nodes_[j].tag.length() - 1] << '\n';
                }
            }
        }
    }

    void Magic() {
        std::vector<size_t> in(nodes_.size(), 0), out(nodes_.size(), 0);

        std::vector<std::vector<std::vector<std::string> > > edges(nodes_.size(),
                                                     std::vector<std::vector<std::string> >(nodes_.size()));
        for (size_t i = 0; i < nodes_.size(); ++i) {
            for (size_t j = 0; j < nodes_.size(); ++j) {
                if (matrix_[i][j]) {
                    std::string s = "";
                    s += nodes_[j].tag[nodes_[j].tag.length() - 1];
                    edges[i][j].push_back(s);
                    in[j]++;
                    out[i]++;
                }
            }
        }
        for (size_t i = 0; i < nodes_.size(); ++i) {
            if (in[i] == 1 && out[i] == 1) {
                size_t next, prev;
                for (size_t k = 0; k < nodes_.size(); ++k) {
                    if (matrix_[i][k]) {
                        next = k;
                    }
                    if (matrix_[k][i]) {
                        prev = k;
                    }
                }
                std::string s = edges[prev][i][0] + edges[i][next][0];
                edges[prev][next].push_back(s);
            }
        }
        for (size_t i = 0; i < nodes_.size(); ++i) {
            for (size_t j = 0; j < nodes_.size(); ++j) {
                for (size_t k = 0; k < edges[i][j].size(); ++k) {
                    std::cout << nodes_[i].tag << edges[i][j][k] << ' ';
                }
            }
        }
    }

private:
    bool DFS(size_t index, std::vector<Node> &path, std::vector<bool> &was) {
        /*std::cout << index << '\n';
        was[index] = true;
        for (size_t i = 0; i < path.size(); ++i) {
            std::cout << path[i].tag << ' ';
        }
        std::cout << '\n';*/
        for (size_t i = 0; i < nodes_.size(); ++i) {
            if (matrix_[index][i] && !was[i]) {
                path.push_back(nodes_[i]);
                if (path.size() == nodes_.size() || DFS(i, path, was)) {
                    return true;
                } else {
                    path.pop_back();
                    was[index] = false;
                    return false;
                }
            }
        }
    }

    void Step(std::vector<std::vector<int>> &matrix, std::vector<Node> &path, size_t index) {
        for (size_t i = 0; i < nodes_.size(); ++i) {
            if (matrix[index][i]) {
                matrix[index][i] = false;
                Step(matrix, path, i);
            }
        }
        path.push_back(nodes_[index]);
    }

    std::vector<std::vector<int> > matrix_;
    std::vector<Node> nodes_;
};

std::vector<Node> GenerateNodes(size_t n) {
    std::vector<Node> nodes;
    for (size_t i = 0; i < (1 << n); ++i) {
        std::string s = "";
        size_t k = i;
        for (size_t j = 0; j < n; ++j) {
            s += '0' + k % 2;
            k /= 2;
        }
        nodes.push_back({0, s});
    }
    return nodes;
}

std::string ConstructString(std::vector<Node> nodes, size_t k, size_t d) {
    std::string ans;
    std::vector<char> chars(1000000);
    size_t count = 0;
    std::vector<std::string> node_strings;
    for (Node node : nodes) {
        node_strings.push_back(node.tag);
    }
    for (std::string s : node_strings) {
        for (size_t j = 0; j < s.length() + d - 1; ++j) {
            if (j < k) {
                chars[count + j] = s[j];
            }
            if (j >= k + d) {
                chars[count + j] = s[j - d + 1];
            }
        }
        count ++;
    }
    for (size_t i = 0; i < 2 * k + d + count - 1; ++i) {
        ans += chars[i];
    }
    return ans;
}

int main() {
    freopen("input.txt", "r", stdin);
    std::string line;
    std::set<std::string> nodes_set;
    std::map<std::string, size_t> nodes_map;
    std::vector<Node> nodes;
    std::vector<std::string> edges;
    while (std::cin >> line) {
        std::string out = line.substr(0, line.size() - 1);
        std::string in = line.substr(1, line.size() - 1);
        nodes_set.insert(out);
        nodes_set.insert(in);
        edges.push_back(line);
    }
    for (auto i = nodes_set.begin(); i != nodes_set.end(); ++i) {
        nodes_map[*i] = nodes.size();
        nodes.push_back({0, *i});
    }
    std::vector<std::vector<int> > matrix(nodes.size(), std::vector<int>(nodes.size(), 0));
    for (auto edge: edges) {
        std::string out = edge.substr(0, edge.size() - 1);
        std::string in = edge.substr(1, edge.size() - 1);
        matrix[nodes_map[out]][nodes_map[in]]++;
    }
    Graph g(matrix, nodes);
    g.Magic();
}
