#include <iostream>
#include <vector>

const std::vector<size_t> masses =
    { 57, 71, 87, 97, 99, 101, 103, 113, 114, 115, 128, 129, 131, 137, 147, 156, 163, 186 };

int main() {
    long long mass;
    std::cin >> mass;
    std::vector<long long> dp(mass + 1, 0);
    dp[0] = 1;
    for (long long i = 0; i < dp.size(); ++i) {
        for (long long cur_mass : masses) {
            if (i - cur_mass >= 0) {
                dp[i] += dp[i - cur_mass];
            }
        }
    }
    std::cout << dp[mass];
}