#include <iostream>
#include <string>

size_t CalculateHammingDistance(std::string &first, std::string &second) {
    size_t res = 0;
    for (size_t i = 0; i < first.size(); ++i) {
        if (first[i] != second[i]) {
            ++res;
        }
    }
    return res;
}

size_t CalculateMinDistance(std::string &pattern, std::string &line) {
    size_t res = -1;
    for (size_t i = 0; i < line.length() + 1 - pattern.length(); ++i) {
        std::string s = line.substr(i, pattern.length());
        res = std::min(res, CalculateHammingDistance(s, pattern));
    }
    return res;
}

int main() {
    freopen("input.txt", "r", stdin);
    std::string pattern, line;
    std::cin >> pattern;

    std::string s;
    size_t ans = 0;
    char c;
    c = getchar();
    while (c != EOF) {
        if (c == ' ') {
            ans += CalculateMinDistance(pattern, s);
            s = "";
        } else {
            s += c;
        }
        c = getchar();
    }
    ans += CalculateMinDistance(pattern, s);
    std::cout << '\n' << ans;
}