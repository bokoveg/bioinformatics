#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <unordered_set>

std::vector<int> CalculatePrefixFunction(std::string line) {
    std::vector<int> prefix_function_value(line.length());
    for (size_t i = 1; i < line.length(); ++i) {
        int last_value = prefix_function_value[i - 1];
        while (last_value > 0 && line[i] != line[last_value]) {
            last_value = prefix_function_value[last_value - 1];
        }
        if (line[last_value] == line[i]) {
            ++last_value;
        }
        prefix_function_value[i] = last_value;
    }
    return prefix_function_value;
}

std::vector<int> FindPattern(std::string &genom, std::string &pattern) {
    std::vector<int> positions;
    std::string line = pattern + '#' + genom;
    std::vector<int> preffix_function = CalculatePrefixFunction(line);
    for (size_t i = pattern.length() + 1; i < preffix_function.size(); ++i) {
        if (preffix_function[i] == pattern.size()) {
            positions.push_back(i - pattern.length() - 1);
        }
    }
    return positions;
}

int main() {
    freopen("input.txt", "r", stdin);
    std::string genom;
    std::unordered_set<std::string> answer, was;
    int pattern_length, search_range, times_found;

    std::cin >> genom;
    std::cin >> pattern_length >> search_range >> times_found;

    for (int i = 0; i < genom.length() - search_range + 1; ++i) {
        std::string cur_pattern;
        for (int j = i; j < i + pattern_length; ++j) {
            cur_pattern += genom[j];
        }
        if (was.find(cur_pattern) != was.end()) {
            continue;
        }
        std::vector<int> positions = FindPattern(genom, cur_pattern);
        for (int j = 0; j + times_found < positions.size() + 1; ++j) {
            if (positions[j + times_found - 1] - positions[j] + pattern_length - 1 <= search_range) {
                answer.insert(cur_pattern);
                break;
            }
        }
    }
    for (auto i = answer.begin(); i != answer.end(); ++i) {
        std::cout << *i << ' ';
    }
}